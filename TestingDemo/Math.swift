//
//  Math.swift
//  TestingDemo
//
//  Created by James Cash on 18-05-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import Foundation

// this is also a naive way of doing caching
// because we never remove values from the cache
// a "real" approach is to use a cache that will eventually remove elements
// something like a TTL (time-to-live) or LRU (Least-Recently-Used)
var cache: [Int:Int] = [0: 1, 1: 1]

struct Math {
    static func naiveFib(_ n: Int) -> Int {
        if n == 0 || n == 1 { return 1 }
        return fib(n-1) + fib(n - 2)
    }

    static func memoziedFib(_ n: Int) -> Int {
        if let hit = cache[n] {
            return hit
        } else {
            let res = fib(n-1) + fib(n-2)
            cache[n] = res
            return res
        }
    }

    static func fib(_ n: Int) -> Int {
        let sqrt5 = sqrt(5)
        return Int( (1/sqrt5) * pow(((1 + sqrt5)/2), Double(n)) -
            (1/sqrt5) * pow(((1 - sqrt5)/2), Double(n))
        )
    }
}
