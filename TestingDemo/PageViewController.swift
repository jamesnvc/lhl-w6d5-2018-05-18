//
//  PageViewController.swift
//  TestingDemo
//
//  Created by James Cash on 18-05-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController {

    var pages: [UIViewController]!

    override func viewDidLoad() {
        super.viewDidLoad()

        dataSource = self

        pages = [storyboard!.instantiateViewController(withIdentifier: "viewControllerOne"),
                 storyboard!.instantiateViewController(withIdentifier: "viewControllerTwo"),
                 storyboard!.instantiateViewController(withIdentifier: "viewControllerThree")
        ]

        setViewControllers([pages[0]], direction: .forward, animated: false, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = pages.index(of: viewController),
            index > 0 else {
                return nil
        }
        return pages[index - 1];
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = pages.index(of: viewController),
            index < pages.count-1 else {
                return nil
        }
        return pages[index + 1];
    }


}
